__Bitcoin Whitepaper__

The Bitcoin whitepaper was included in the original Bitcoin project files with the project clearly published under the MIT license by Satoshi Nakamoto.

*The sole purpose of this deposit is to keep a copy of the original white paper.*

- Topic: https://bitcoin.org/en/posts/regarding-csw
- Discussion: https://github.com/bitcoin-core/bitcoincore.org/pull/740
- Archive: http://sourceforge.net/projects/bitcoin

